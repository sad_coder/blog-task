import {createRouter, createWebHistory, RouteRecordRaw} from 'vue-router'
import HomeView from '../views/HomeView.vue'

const routes: Array<RouteRecordRaw> = [
    {
        path: '/',
        name: 'home',
        component: import('../views/PostsView.vue')
    },
    {
        path: '/login',
        name: 'login',
        component: () => import( '../views/LoginView.vue')
    },
    {
        path: '/register',
        name: 'register',
        component: () => import( '../views/RegisterView.vue')
    },
    {
        path: '/posts',
        name: 'posts',
        component: () => import( '../views/PostsView.vue')
    },
    {
        path: '/post/:id(\\d+)',
        name: 'post',
        component: () => import('../views/PostView.vue')
    },
    {
        path: '/post/store',
        name: 'create_post',
        component: () => import('../views/PostCreateView.vue')
    },
    {
        path: '/post/update/:id(\\d+)',
        name: 'update_post',
        component: () => import('../views/PostUpdateView.vue')
    },

]

const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes
})

export default router
