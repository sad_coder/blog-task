import {Ref, ref} from "vue";

// Modules
import api from "@/api/api";

import {
    IUser, IUserRegister,
} from "@/models/user.model"


export const useAuth = () => {
    // Model Variables
    const emailModel = ref<string>('')
    const passwordModel = ref<string>('')
    const userData: Ref<IUser | null> = ref(null)
    const registerData: Ref<IUserRegister> = ref({
        name: "",
        email: "",
        birthday: "",
        password: "",
        password_confirmation: ""
    });

    const login = async (): Promise<IUser> => {
        return (await api.auth.login({
            email: emailModel.value,
            password: passwordModel.value
        })).data
    }

    const register = async (): Promise<IUser> => {
        return (await api.auth.register(registerData.value)).data
    }

    return {
        userData,
        loginUser: login,
        emailModel,
        passwordModel,
        registerData,
        register
    }
}
