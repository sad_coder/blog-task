import {Ref, ref} from "vue";

// Modules
import api from "@/api/api";

import {IPost, IPostCreate, IPostsResponse,} from "@/models/post.model"
import {IPagination,} from "@/models/pagination.model"


export const usePosts = () => {
    // Model Variables
    const posts = ref<IPost[]>([])
    const post = ref<IPost | null>(null);
    const createPostData = ref<IPostCreate>({
        title: "",
        body: ""
    });

    const updatePostData = ref<IPostCreate>({
        title: "",
        body: ""
    });

    const pagination = ref<IPagination>({
        page: 1,
        total: 10,
        by: 10,
    });

    const getPosts = async (page: number): Promise<IPostsResponse> => {
        const resp = (await api.post.index({page: page})).data;
        posts.value = resp.posts;
        pagination.value = resp.pagination;
        return resp;
    }

    const getPost = async (id: number): Promise<IPost> => {
        const resp = (await api.post.show(id)).data;
        post.value = resp.post;
        return resp.post;
    }

    const deletePost = async (id: number): Promise<number> => {
        const resp = (await api.post.delete(id));
        return resp.status;
    }

    const storePost = async (): Promise<number> => {
        const resp = (await api.post.store(createPostData.value));
        return resp.status;
    }

    const updatePost = async (id: number): Promise<number> => {
        const resp = (await api.post.update(updatePostData.value, id));
        return resp.status;
    }

    return {
        posts,
        pagination,
        getPosts,
        getPost,
        post,
        deletePost,
        createPostData,
        storePost,
        updatePost,
        updatePostData
    }
}
