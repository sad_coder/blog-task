import { defineStore } from 'pinia';
import {IUser} from "@/models/user.model";

interface UserState {
    token: string | null;
    userInfo: IUser | null;
}

export const useUserStore = defineStore('user', {
    state: (): UserState => ({
        token: localStorage.getItem('token') || null,
        userInfo: JSON.parse(localStorage.getItem('userInfo') || '{}') || null,
    }),
    actions: {
        setToken(token: string) {
            this.token = token;
            localStorage.setItem('token', token);
        },
        clearToken() {
            this.token = null;
            localStorage.removeItem('token');
        },
        setUserInfo(userInfo: IUser) {
            this.userInfo = userInfo;
            localStorage.setItem('userInfo', JSON.stringify(userInfo));
        },
        clearUserInfo() {
            this.userInfo = null;
            localStorage.removeItem('userInfo');
        },
        logout() {
            this.clearToken();
            this.clearUserInfo();
        },
    },
});
