import {auth} from "@/api/nodes/auth";
import {post} from "@/api/nodes/post";


export const api = {
    auth,
    post
};

export default api;
