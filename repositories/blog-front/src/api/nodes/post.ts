import {IPostCreate, IPostUpdate} from "@/models/post.model";

const END_POINT_URL = "/post";
import {clientHttp} from "../clientHttp";

export const post = {
    index: (params: object) => clientHttp.get(END_POINT_URL + "/", {params: params}),

    delete: (id: number) => clientHttp.delete(END_POINT_URL + "/" + id),

    update: (params: IPostUpdate, id:number) => clientHttp.put(END_POINT_URL + "/" + id, params),

    show: (id: number) => clientHttp.get(END_POINT_URL + "/" + id),

    store: (params: IPostCreate) => clientHttp.post(END_POINT_URL + "/", params),

};