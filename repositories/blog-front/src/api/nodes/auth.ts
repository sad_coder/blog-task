const END_POINT_URL = "/auth";
import {clientHttp} from "../clientHttp";

export const auth = {
    login: (params: object) => clientHttp.post(END_POINT_URL + "/login", params),

    register: (params: object) =>
        clientHttp.post(END_POINT_URL + "/register", params),
};