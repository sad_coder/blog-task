import axios, {AxiosRequestConfig, InternalAxiosRequestConfig} from "axios";
const getAuthToken = () => localStorage.getItem("token");


const config: AxiosRequestConfig = {
    baseURL: "http://localhost:7777/api",
    headers: {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*",
    },
};

const authInterceptor = (config: AxiosRequestConfig): InternalAxiosRequestConfig => {
    const token = getAuthToken();
    if (token) {
        if (!config.headers) {
            config.headers = {};
        }
        (config.headers as any)["Authorization"] = `Bearer ${token}`;
    }
    return config as InternalAxiosRequestConfig;
};

export const clientHttp = axios.create(config);


clientHttp.interceptors.request.use(authInterceptor, (error) => Promise.reject(error));

export default clientHttp;
