import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue-3/dist/bootstrap-vue-3.css';
import BootstrapVue3 from 'bootstrap-vue-3';
import {createPinia} from "pinia";
import { library } from '@fortawesome/fontawesome-svg-core';
import { faTrash, faEdit, faEye } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';

library.add(faTrash, faEdit, faEye);


const app = createApp(App);

const pinia = createPinia();

app.use(BootstrapVue3);
app.use(pinia);

app.use(router).component('font-awesome-icon', FontAwesomeIcon).mount('#app')
