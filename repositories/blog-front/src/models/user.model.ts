import {IToken} from "@/models/token.model";

export interface IUser {
    id:number,
    name:string,
    email:string,
    password:string,
    birthday?: string | null,
    token_data:IToken
}

export interface IUserRegister {
    name:string,
    email:string,
    password:string,
    password_confirmation:string,
    birthday?: string | null,
}