export interface IPost {
    title:string,
    body:string,
    user_id:number,
    created_at:string,
    author_name:string
}

export interface IPagination {
    page:number,
    by:number,
    total:number,
}

export interface IPostsResponse {
    pagination:IPagination,
    posts:IPost[]
}

export interface IPostCreate {
    title:string,
    body:string
}

export interface IPostUpdate {
    title?: string;
    body?: string;
}