export interface IPagination {
    page:number,
    by:number,
    total:number,
}