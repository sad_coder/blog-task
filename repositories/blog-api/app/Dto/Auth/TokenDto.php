<?php

namespace App\Dto\Auth;

use Spatie\LaravelData\Dto;
use Illuminate\Support\Carbon;

class TokenDto extends Dto
{
    /**
     * @param string|null                     $token
     * @param string|null                     $token_type
     * @param \Illuminate\Support\Carbon|null $expired_at
     */
    public function __construct(
        public ?string $token,
        public ?string $token_type,
        public ?Carbon $expired_at
    ) {
    }
}
