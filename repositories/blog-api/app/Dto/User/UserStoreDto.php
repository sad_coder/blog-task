<?php

namespace App\Dto\User;

use App\Dto\Auth\TokenDto;
use Spatie\LaravelData\Data;
use Spatie\LaravelData\Optional;

class UserStoreDto extends Data
{
    /**
     * @param string                                              $name
     * @param string                                              $email
     * @param string                                              $password
     * @param string|\Spatie\LaravelData\Optional                 $birthday
     * @param \Spatie\LaravelData\Optional|\App\Dto\Auth\TokenDto $token_data
     */
    public function __construct(
        public string $name,
        public string $email,
        public string $password,
        public string|Optional $birthday,
        public Optional|TokenDto $token_data,
        public Optional|int $id
    ) {
    }
}
