<?php

namespace App\Dto\User;

use DateTime;
use Spatie\LaravelData\Data;
use Spatie\LaravelData\Optional;
use Spatie\LaravelData\Attributes\WithCast;
use Spatie\LaravelData\Casts\DateTimeInterfaceCast;

class UserShowDto extends Data
{
    public function __construct(
        public int $id,
        public string $name,
        public string $email,
        #[WithCast(DateTimeInterfaceCast::class, format: 'Y-m-d')]
        public DateTime|null $birthday,
    ) {
    }
}
