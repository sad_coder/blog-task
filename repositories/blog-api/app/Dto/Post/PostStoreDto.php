<?php

namespace App\Dto\Post;

use Spatie\LaravelData\Data;
use Spatie\LaravelData\Attributes\MapName;

class PostStoreDto extends Data
{
    public function __construct(
        #[MapName('user_id')]
        public int $userId,
        #[MapName('dummy_post_id')]
        public int $id,
    ) {

    }
}
