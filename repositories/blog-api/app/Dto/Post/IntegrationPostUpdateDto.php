<?php

namespace App\Dto\Post;

use Spatie\LaravelData\Data;

class IntegrationPostUpdateDto extends Data
{
    /**
     * @param string $title
     * @param string $body
     */
    public function __construct(
        public string $title,
        public string $body,
    ) {
    }
}
