<?php

namespace App\Dto\Post;

use Spatie\LaravelData\Data;

class IntegrationPostStoreDto extends Data
{
    /**
     * @param string $title
     * @param string $body
     * @param int    $userId
     */
    public function __construct(
        public string $title,
        public string $body,
        public int $userId
    ) {
    }
}
