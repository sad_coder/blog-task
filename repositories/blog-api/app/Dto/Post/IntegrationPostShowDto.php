<?php

namespace App\Dto\Post;

use Spatie\LaravelData\Data;
use Spatie\LaravelData\Optional;


class IntegrationPostShowDto extends Data
{
    public function __construct(
        public string $title,
        public string $body,
        public int $id,
        public string|Optional $author_name
    ) {
    }
}
