<?php

namespace App\Dto\Post;

use Illuminate\Support\Str;
use Spatie\LaravelData\Data;
use Illuminate\Support\Optional;

class PostShowDto extends Data
{
    public function __construct(
        public int $id,
        public int $user_id,
        public string $created_at,
        public int $dummy_post_id,
        public string $title,
        public string $body,
        public string|Optional $author_name,
    ) {
        $this->body = Str::limit($this->body, 128);
    }
}
