<?php

namespace App\Dto\Post;

use Spatie\LaravelData\Data;
use Spatie\LaravelData\Optional;
use Spatie\LaravelData\Attributes\MapName;

class PostDto extends Data
{
    /**
     * @param int                                 $id
     * @param int                                 $user_id
     * @param string                              $created_at
     * @param int                                 $dummy_post_id
     * @param string|\Spatie\LaravelData\Optional $author_name
     */
    public function __construct(
        public int $id,
        public int $user_id,
        public string $created_at,
        public int $dummy_post_id,
        public string|Optional $author_name
    ) {
    }
}
