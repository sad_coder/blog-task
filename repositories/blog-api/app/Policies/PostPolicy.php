<?php

namespace App\Policies;

use App\Models\Post;
use App\Models\User;
use App\Exceptions\UnauthorizedActionException;
use Illuminate\Auth\Access\HandlesAuthorization;

class PostPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can update the model.
     *
     * @throws \App\Exceptions\UnauthorizedActionException
     */
    public function update(User $user, Post $post): bool
    {

        if ($user->id !== $post->user_id) {
            throw new UnauthorizedActionException('You are not allowed to update posts.');
        }

        return true;
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @throws \App\Exceptions\UnauthorizedActionException
     */
    public function delete(User $user, Post $post): bool
    {
        if ($user->id !== $post->user_id) {
            throw new UnauthorizedActionException('You are not allowed to delete posts.');
        }

        return true;
    }

}
