<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use app\Repositories\Post\PostRepository;
use app\Repositories\User\UserRepository;
use app\Repositories\Post\Interfaces\PostRepositoryInterface;
use app\Repositories\User\Interfaces\UserRepositoryInterface;

class RepositoryBindingServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        $this->app->bind(PostRepositoryInterface::class, PostRepository::class);
        $this->app->bind(UserRepositoryInterface::class, UserRepository::class);
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        //
    }
}
