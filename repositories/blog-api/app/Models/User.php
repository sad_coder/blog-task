<?php

namespace App\Models;

use Firebase\JWT\JWT;
use App\Dto\Auth\TokenDto;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable implements JWTSubject
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'birthday'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'password' => 'hashed',
    ];

    public function getJWTIdentifier(): mixed
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims(): array
    {
        return [];
    }

    /**
     * @return \App\Dto\Auth\TokenDto
     */
    public function jwt(): TokenDto
    {
        $expiredAt = now()->addSeconds(config('jwt-auth.ttl'));
        $issuedAtTime = now();

        $payload = array_merge($this->getJWTCustomClaims(), [
            'iat' => $issuedAtTime->timestamp,
            'exp' => $expiredAt->timestamp,
        ]);

        return new TokenDto(
            JWT::encode($payload, config('jwt.secret'), config('jwt.algo')),
            'bearer',
            $expiredAt
        );
    }
}
