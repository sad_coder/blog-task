<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Http\JsonResponse;

class ModelNotFoundException extends Exception
{
    /**
     * @param $message
     */
    public function __construct($message = "Model not found")
    {
        parent::__construct($message);
        $this->message = $message;
    }

    /**
     * Render the exception into an HTTP response.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function render(): JsonResponse
    {
        return response()->json([
            'message' => $this->message,
        ], 404);
    }
}
