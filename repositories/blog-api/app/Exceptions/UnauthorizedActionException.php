<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class UnauthorizedActionException extends Exception
{
    protected $message;

    public function __construct($message = "Unauthorized action")
    {
        parent::__construct($message);
        $this->message = $message;
    }

    /**
     * Render the exception into an HTTP response.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function render(): JsonResponse
    {
        return response()->json(['message' => $this->message], Response::HTTP_FORBIDDEN);
    }
}
