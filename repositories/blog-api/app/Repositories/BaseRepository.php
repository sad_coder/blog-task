<?php

namespace App\Repositories;

use Throwable;
use App\Exceptions\DatabaseException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class BaseRepository
 *
 * @package App\Repositories
 */
abstract class BaseRepository
{
    public function __construct()
    {
        $this->setModel();
    }

    /**
     * @var string|Model
     */
    protected string|Model $model;

    /**
     * @return void
     */
    abstract protected function setModel(): void;


}
