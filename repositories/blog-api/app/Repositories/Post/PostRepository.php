<?php

namespace app\Repositories\Post;

use App\Models\Post;
use App\Dto\Post\PostDto;
use App\Dto\Post\PostStoreDto;
use App\Repositories\BaseRepository;
use App\Exceptions\ModelNotFoundException;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use app\Repositories\Post\Interfaces\PostRepositoryInterface;

class PostRepository extends BaseRepository implements PostRepositoryInterface
{

    protected function setModel(): void
    {
        $this->model = Post::class;
    }

    /**
     * @param \App\Dto\Post\PostStoreDto $data
     *
     * @return \App\Dto\Post\PostStoreDto
     */
    public function store(PostStoreDto $data): PostStoreDto
    {
        $data = $this->model::query()->create(
            $data->toArray()
        );

        return PostStoreDto::from($data->toArray());
    }

    /**
     * @param int $id
     *
     * @return void
     */
    public function changeUpdatedTime(
        int $id
    ): void {
        $this->model::query()->where("id", $id)->update([
            "updated_at" => now()->addSecond(1)
        ]);
    }

    /**
     * @param int $id
     *
     * @return bool
     */
    public function delete(int $id): bool
    {
        $model = $this->model::query()->findOrFail($id);

        return $model->delete();
    }

    /**
     * @param int $id
     *
     * @return \App\Dto\Post\PostDto
     * @throws \App\Exceptions\ModelNotFoundException
     */
    public function findByIdWithAuthorName(int $id): PostDto
    {
        $post = $this->model::query()
            ->join('users', 'users.id', '=', 'posts.user_id')
            ->select('posts.*', 'users.name as author_name')
            ->find($id);

        if (!$post) {
            throw new ModelNotFoundException('Post not found');
        }

        return PostDto::from($post->toArray());
    }

    public function getWithPagination(
        int $perPage
    ): LengthAwarePaginator {
        return $this->model::query()
            ->join('users', 'users.id', '=', 'posts.user_id')
            ->select('posts.*', 'users.name as author_name')
            ->orderBy('created_at', 'desc')
            ->paginate($perPage);
    }

}
