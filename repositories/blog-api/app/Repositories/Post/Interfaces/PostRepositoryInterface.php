<?php

namespace app\Repositories\Post\Interfaces;

use App\Dto\Post\PostDto;
use App\Dto\Post\PostStoreDto;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

interface PostRepositoryInterface
{
    /**
     * @param \App\Dto\Post\PostStoreDto $data
     *
     * @return \App\Dto\Post\PostStoreDto
     */
    public function store(PostStoreDto $data): PostStoreDto;

    /**
     * @param int $id
     *
     * @return void
     */
    public function changeUpdatedTime(int $id): void;

    /**
     * @param int $id
     *
     * @return bool
     */
    public function delete(int $id): bool;

    /**
     * @param int $id
     *
     * @return \App\Dto\Post\PostDto
     */
    public function findByIdWithAuthorName(int $id): PostDto;

    /**
     * @param int $perPage
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function getWithPagination(int $perPage): LengthAwarePaginator;
}
