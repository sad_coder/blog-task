<?php

namespace app\Repositories\User;

use App\Models\User;
use App\Dto\User\UserStoreDto;
use App\Repositories\BaseRepository;
use app\Repositories\User\Interfaces\UserRepositoryInterface;

class UserRepository extends BaseRepository implements UserRepositoryInterface
{

    protected function setModel(): void
    {
        $this->model = User::class;
    }

    /**
     * @param \App\Dto\User\UserStoreDto $data
     *
     * @return \App\Dto\User\UserStoreDto
     */
    public function store(UserStoreDto $data): UserStoreDto
    {

        $user = $this->model::query()->create($data->toArray());

        return UserStoreDto::from([
            ...$data->toArray(),
            'token_data' => $user->jwt()
        ]);
    }

}
