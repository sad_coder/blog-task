<?php

namespace app\Repositories\User\Interfaces;

use App\Dto\User\UserStoreDto;

interface UserRepositoryInterface
{
    /**
     * @param \App\Dto\User\UserStoreDto $data
     *
     * @return \App\Dto\User\UserStoreDto
     */
    public function store(UserStoreDto $data) :UserStoreDto;
}
