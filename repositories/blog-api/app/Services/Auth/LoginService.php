<?php

namespace App\Services\Auth;

use App\Dto\Auth\LoginDto;
use App\Dto\User\UserAuthDto;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\AuthenticationException;

class LoginService
{
    /**
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function login(LoginDto $data): UserAuthDto
    {

        if (!$token = JWTAuth::attempt($data->toArray())) {
            throw new AuthenticationException('invalid_credentials');
        }

        return UserAuthDto::from([
            'token' => $token,
            ...Auth::user()->toArray()
        ]);
    }
}
