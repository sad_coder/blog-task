<?php

namespace App\Services\Auth;

use App\Dto\User\UserStoreDto;
use app\Repositories\User\Interfaces\UserRepositoryInterface;

class RegisterService
{
    public function __construct(
        private readonly UserRepositoryInterface $userRepository
    ) {
    }

    /**
     * @param \App\Dto\User\UserStoreDto $data
     *
     * @return \App\Dto\User\UserStoreDto
     */
    public function register(UserStoreDto $data): UserStoreDto
    {
        return $this->userRepository->store($data);
    }

}
