<?php

namespace App\Services\StorageApiIntegration;

use GuzzleHttp\Promise\Utils;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Redis;
use App\Dto\Post\IntegrationPostShowDto;
use App\Dto\Post\IntegrationPostStoreDto;
use App\Dto\Post\IntegrationPostUpdateDto;
use Symfony\Component\HttpFoundation\Response;
use app\Services\StorageApiIntegration\Interfaces\StorageApiIntegrationInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Promise;

class DummyStorageIntegrationService extends BaseApiStorageIntegrationService implements StorageApiIntegrationInterface
{

    /**
     * @return void
     */
    protected function setApiUrl(): void
    {
        $this->apiUrl = config('post_storage.dummy.api_url');
    }

    /**
     * @return void
     */
    protected function setHttpClient(): void
    {
        $this->httpClient = new Client();
    }

    /**
     * @throws \Exception
     */
    public function storePost(IntegrationPostStoreDto $data): int
    {
        $storeRequest = $this->httpClient
            ->post($this->apiUrl . 'posts/add', [
                'json' => $data->toArray()
            ]);

        if ($storeRequest->getStatusCode() === Response::HTTP_CREATED) {
            $responseData = json_decode($storeRequest->getBody()->getContents());
            return $responseData->id;
        } else {
            throw new \Exception("Error: " . $storeRequest->getStatusCode());
        }
    }

    /**
     * @param int $postId
     *
     * @return \App\Dto\Post\IntegrationPostShowDto
     * @throws \Exception
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getById(int $postId): IntegrationPostShowDto
    {
        $showRequest = $this->httpClient
            ->get($this->apiUrl . 'posts/' . $postId);

        if ($showRequest->getStatusCode() !== Response::HTTP_OK) {
            throw new \Exception("Error: " . $showRequest->getStatusCode());
        }

        return IntegrationPostShowDto::from(json_decode($showRequest->getBody(), true));
    }

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Exception
     */
    public function updateById(IntegrationPostUpdateDto $data, int $postId): IntegrationPostUpdateDto
    {
        $updateRequest = $this->httpClient->put($this->apiUrl . 'posts/' . $postId, $data->toArray());

        if ($updateRequest->getStatusCode() !== Response::HTTP_OK) {
            throw new \Exception("Error: " . $updateRequest->getStatusCode());
        }

        return IntegrationPostUpdateDto::from(json_decode($updateRequest->getBody(), true));
    }

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Exception
     */
    public function deleteById(int $postId): void
    {
        $deleteRequest = $this->httpClient->delete($this->apiUrl . 'posts/' . $postId);

        if ($deleteRequest->getStatusCode() !== Response::HTTP_OK) {
            throw new \Exception("Error: " . $deleteRequest->getStatusCode());
        }
    }

    public function getByIds(array $postIds)
    {
        $promises = [];

        foreach ($postIds as $index => $postId) {
            //@todo сюда вставляю просто 1 потому что dummy при создании всегда дает пост с айди 252 и не возвращает его
            // а есть в его базе только 251 пост как я предпологаю
            $cachedPost = Redis::get('post_dummy:' . 1);
            if ($cachedPost) {
                $promises[$index] = $cachedPost;
            } else {
                $response = $this->httpClient->get($this->apiUrl . 'posts/' . 1);
                $promises[$index] = json_decode($response->getBody()->getContents(), true);
                $this->cacheOnePost(IntegrationPostShowDto::from($promises[$index]));
            }
        }

        $responses = Utils::settle($promises)->wait();
        $results = [];

        foreach ($responses as $postId => $response) {
            if ($response['state'] === 'fulfilled') {
                $results[$postId] = $response['value'];
            } else {
                // @todo тут можно выбрасывать exception а можно null и делать например на фронте заглушку пост не
                // найден
                // и в джобе как-то обрабатывать и удалять его из базы чтобы не получилось так что весь запрос умрет
                // от 1 ненайденного у провайдера поста
                $results[$postId] = null;
            }
        }

        return IntegrationPostShowDto::collect($results);
    }

    /**
     * @param \App\Dto\Post\IntegrationPostShowDto $data
     *
     * @return void
     */
    protected function cacheOnePost(
        IntegrationPostShowDto $data
    ): void {
        //чтобы быстро очистить кэш, можно еще просто через скан достать айди но я заюзаю множество чтобы не задеть
        // лишнего если нужно чистить весь кэш постов резко
        Redis::sAdd("tag:post_dummy", $data->id);
        Redis::set('post_dummy:' . $data->id, json_encode($data->toArray()));
        // при каждом получении поста обновляю время последнего просмотра чтобы не хранить в кэше старые посты
        Redis::zAdd('posts:last_viewed', time(), $data->id);
    }
}
