<?php

namespace app\Services\StorageApiIntegration\Interfaces;

use App\Dto\Post\IntegrationPostStoreDto;
use App\Dto\Post\IntegrationPostUpdateDto;

interface StorageApiIntegrationInterface
{

    /**
     * @param \App\Dto\Post\IntegrationPostStoreDto $data
     *
     * @return int
     */
    public function storePost(IntegrationPostStoreDto $data): int;

    /**
     * @param int $postId
     *
     * @return mixed
     */
    public function getById(int $postId);

    /**
     * @param array $postIds
     *
     * @return mixed
     */
    public function getByIds(array $postIds);

    /**
     * @param \App\Dto\Post\IntegrationPostUpdateDto $data
     * @param int                                    $postId
     *
     * @return mixed
     */
    public function updateById(IntegrationPostUpdateDto $data, int $postId);

    /**
     * @param int $postId
     *
     * @return mixed
     */
    public function deleteById(int $postId);

}
