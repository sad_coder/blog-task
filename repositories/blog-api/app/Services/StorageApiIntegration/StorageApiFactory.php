<?php

namespace App\Services\StorageApiIntegration;

use InvalidArgumentException;
use app\Services\StorageApiIntegration\Interfaces\StorageApiIntegrationInterface;

class StorageApiFactory
{
    public static function getStorageService(
        string $type
    ): StorageApiIntegrationInterface {
        return match ($type) {
            // @todo пока я просто сюда делаю другую константу но может быть например запасной сервис апи интеграции
            // на который можно будет по фасту переключиться если этот упадет
            config('post_storage.active_service')   => new DummyStorageIntegrationService(),
            config('post_storage.fallback_service') => new DummyStorageIntegrationService(),
            default                                 => throw new InvalidArgumentException(
                "Неизвестный тип интеграции: $type"
            ),
        };
    }
}
