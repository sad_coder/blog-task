<?php

namespace App\Services\StorageApiIntegration;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Http;
use App\Dto\Post\IntegrationPostShowDto;
use Illuminate\Http\Client\PendingRequest;

abstract class BaseApiStorageIntegrationService
{
    /**
     * @var string
     */
    protected string $apiUrl;

    /**
     * @var \GuzzleHttp\Client
     */
    protected Client $httpClient;

    public function __construct()
    {
        $this->setApiUrl();
        $this->setHttpClient();
    }

    /**
     * @return void
     */
    abstract protected function setApiUrl(): void;

    abstract protected function setHttpClient(): void;

    /**
     * @param \App\Dto\Post\IntegrationPostShowDto $data
     *
     * @return void
     */
    abstract protected function cacheOnePost(IntegrationPostShowDto $data): void;
}
