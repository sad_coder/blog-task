<?php

namespace App\Services\Post;

use App\Dto\Post\PostShowDto;
use App\Dto\Post\PostStoreDto;
use Illuminate\Support\Collection;
use App\Dto\Post\IntegrationPostShowDto;
use App\Dto\Post\IntegrationPostStoreDto;
use app\Repositories\Post\PostRepository;
use App\Dto\Post\IntegrationPostUpdateDto;
use App\Services\StorageApiIntegration\StorageApiFactory;
use app\Repositories\Post\Interfaces\PostRepositoryInterface;
use App\Services\StorageApiIntegration\BaseApiStorageIntegrationService;

class PostService
{

    /**
     * @var \App\Services\StorageApiIntegration\BaseApiStorageIntegrationService
     */
    private BaseApiStorageIntegrationService $apiStorageIntegrationService;

    public function __construct(
        private readonly PostRepositoryInterface $postRepository,
    ) {
        $this->apiStorageIntegrationService = StorageApiFactory::getStorageService('dummy');
    }

    /**
     * @throws \Exception
     */
    public function store(IntegrationPostStoreDto $postData): PostStoreDto
    {
        $postId = $this->apiStorageIntegrationService->storePost($postData);

        //@todo dummy_post_id лучше было бы назвать чем-то абстрактным ибо может быть с друго сервиса
        // и например добавить тип сервиса на котором хранится пост но в рамках этой простой тз лишнее

        return $this->postRepository->store(PostStoreDto::from([
            'dummy_post_id' => $postId,
            'user_id' => $postData->userId,
        ]));
    }

    /**
     * @param \App\Dto\Post\IntegrationPostUpdateDto $data
     * @param int                                    $id
     *
     * @return \App\Dto\Post\IntegrationPostUpdateDto
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function update(IntegrationPostUpdateDto $data, int $id): IntegrationPostUpdateDto
    {
        $data = $this->apiStorageIntegrationService->updateById($data, $id);
        $this->postRepository->changeUpdatedTime($id);

        return $data;
    }

    /**
     * @param int $id
     *
     * @return void
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function delete(int $id): void
    {
        $this->postRepository->delete($id);
        $this->apiStorageIntegrationService->deleteById($id);
    }

    /**
     * @param int $postId
     *
     * @return \App\Dto\Post\IntegrationPostShowDto
     * @throws \Exception
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function show(int $postId): IntegrationPostShowDto
    {
        $post = $this->postRepository->findByIdWithAuthorName($postId);

        $postData = $this->apiStorageIntegrationService->getById($postId);


        return IntegrationPostShowDto::from([
            ...$post->toArray(),
            ...$postData->toArray()
        ]);
    }

    /**
     * @param int $perPage
     *
     * @return \Illuminate\Support\Collection
     */
    public function getWithPaginate(
        int $perPage = 10
    ): Collection
    {
        $posts = $this->postRepository->getWithPagination($perPage);
        $ids = collect($posts->items())->pluck('dummy_post_id')->toArray();

        $postsData = $this->apiStorageIntegrationService->getByIds($ids);

        $data =  collect($posts->items())->map(static function ($post, $index) use ($postsData) {
            return PostShowDto::from([
                ...$postsData[$index]->toArray(),
                ...$post->toArray()
            ]);
        });

        $data['pagination'] = [
            'total' => $posts->total(),
            'current_page' => $posts->currentPage(),
            'by' => 10,
        ];
        return $data;
    }

}
