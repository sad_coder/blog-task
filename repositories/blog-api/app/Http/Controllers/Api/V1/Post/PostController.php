<?php

namespace app\Http\Controllers\Api\V1\Post;

use App\Models\Post;
use Illuminate\Http\JsonResponse;
use App\Services\Post\PostService;
use Illuminate\Support\Facades\Auth;
use Knuckles\Scribe\Attributes\Group;
use Knuckles\Scribe\Attributes\UrlParam;
use App\Dto\Post\IntegrationPostStoreDto;
use App\Http\Controllers\BaseController;
use Knuckles\Scribe\Attributes\BodyParam;
use App\Dto\Post\IntegrationPostUpdateDto;
use App\Http\Requests\Post\PostStoreRequest;
use app\Http\Requests\Post\PostUpdateRequest;
use Symfony\Component\HttpFoundation\Response;
use Knuckles\Scribe\Attributes\ResponseFromFile;
#[Group('Post requests')]
class PostController extends BaseController
{

    /**
     * @param \App\Services\Post\PostService $postService
     */
    public function __construct(
        private readonly PostService $postService
    ) {
    }

    /**
     * @param \App\Http\Requests\Post\PostStoreRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    #[BodyParam("body", "string", "The email of the user.", example: '2000-04-24')]
    #[BodyParam("title", "string", "The name of the user.", example: 'sadCoder')]
    #[ResponseFromFile("responses/posts/store/401.json", 401, description: "not authorized")]
    #[ResponseFromFile("responses/posts/store/201.json", 201, description: "Success auth")]
    #[ResponseFromFile("responses/posts/store/422.json", 422, description: "validation errors")]
    public function store(
        PostStoreRequest $request
    ): JsonResponse {
        $data = $this->postService->store(
            IntegrationPostStoreDto::from([
                'userId' => Auth::id(),
                ...$request->validated()
            ])
        );
        return $this
            ->setStatusCode(Response::HTTP_CREATED)
            ->respondWithSuccessV1($data->toArray(), 'success store post', 'post');
    }

    /**
     * @param \App\Models\Post                          $post
     * @param \app\Http\Requests\Post\PostUpdateRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    #[BodyParam("body", "string", "The email of the user.", example: '2000-04-24')]
    #[BodyParam("title", "string", "The name of the user.", example: 'sadCoder')]
    #[ResponseFromFile("responses/posts/update/422.json", 403, description: "not permission")]
    #[ResponseFromFile("responses/posts/update/200.json", 200, description: "Success update")]
    #[ResponseFromFile("responses/posts/update/422.json", 422, description: "validation errors")]
    public function update(
        Post $post,
        PostUpdateRequest $request
    ): JsonResponse {
        $this->authorize('update', $post);

        $data = $this->postService->update(
            IntegrationPostUpdateDto::from($request->validated()),
            $post->id
        );

        return $this->respondWithSuccessV1($data->toArray(), 'success update post', 'post');
    }

    /**
     * @param \App\Models\Post $post
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    #[UrlParam("id", "int", "The id of the post.", example: '12')]
    #[ResponseFromFile("responses/posts/delete/403.json", 403, description: "not permission")]
    #[ResponseFromFile("responses/posts/delete/401.json", 401, description: "not authorized")]
    #[ResponseFromFile("responses/posts/delete/204.json", 204, description: "success delete")]
    public function destroy(
        Post $post
    ): JsonResponse {
        $this->authorize('delete', $post);

        $this->postService->delete($post->id);

        return $this->setStatusCode(Response::HTTP_NO_CONTENT)
            ->respondWithSuccessV1([], 'success delete post', 'post');
    }

    /**
     *
     * @group Post Requests
     *
     * @param int $id
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception|\GuzzleHttp\Exception\GuzzleException
     */
    #[UrlParam("id", "int", "The post's ID.", example: 44)]
    #[ResponseFromFile("responses/posts/show/200.json", Response::HTTP_OK, description: "show one post")]
    #[ResponseFromFile("responses/posts/show/404.json", Response::HTTP_OK, description: "not found")]
    public function show(
        int $id
    ): JsonResponse {
        $post = $this->postService->show($id);

        return $this->respondWithSuccessV1($post->toArray(), 'success get post', 'post');
    }

    /**
     * @group Post Requests
     *
     * @return \Illuminate\Http\JsonResponse
     */
    #[ResponseFromFile("responses/posts/index/200.json", Response::HTTP_OK, description: "posts list")]
    public function index(): JsonResponse
    {
        $posts = $this->postService->getWithPaginate();

        return $this
            ->setCustomPagination($posts['pagination'])
            ->respondWithSuccessV1($posts->toArray(), 'success get post', 'posts');
    }

}
