<?php

namespace app\Http\Controllers\Api\V1\Auth;

use App\Dto\User\UserStoreDto;
use Illuminate\Http\JsonResponse;
use Knuckles\Scribe\Attributes\Group;
use App\Services\Auth\RegisterService;
use App\Http\Controllers\BaseController;
use Knuckles\Scribe\Attributes\BodyParam;
use App\Http\Requests\Auth\RegisterRequest;
use Symfony\Component\HttpFoundation\Response;
use Knuckles\Scribe\Attributes\ResponseFromFile;

#[Group('Auth requests')]
class RegisterController extends BaseController
{

    /**
     * @param \App\Services\Auth\RegisterService $registerService
     */
    public function __construct(
        private readonly RegisterService $registerService
    ) {
    }

    /**
     * @param \App\Http\Requests\Auth\RegisterRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    #[BodyParam("password", "string", "The password of the user.", example: '123456')]
    #[BodyParam("password_confirmation", "string", "The confirmation password of the user.", example: '123456')]
    #[BodyParam("email", "string", "The email of the user.", example: 'sad_coder@gmail.com')]
    #[BodyParam("birthday", "string", "The email of the user.", example: '2000-04-24')]
    #[BodyParam("name", "string", "The name of the user.", example: 'sadCoder')]
    #[ResponseFromFile("responses/auth/register/422.json", 422, description: "incorrect data")]
    #[ResponseFromFile("responses/auth/register/201.json", 201, description: "Success auth")]
    public function register(
        RegisterRequest $request
    ): JsonResponse {
        $data = $this->registerService->register(
            UserStoreDto::from($request->validated())
        );

        return $this
            ->setStatusCode(Response::HTTP_CREATED)
            ->respondWithSuccessV1($data->toArray(), 'success register', 'user');
    }

}
