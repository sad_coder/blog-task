<?php

namespace app\Http\Controllers\Api\V1\Auth;

use App\Dto\Auth\LoginDto;
use Illuminate\Http\JsonResponse;
use App\Services\Auth\LoginService;
use Knuckles\Scribe\Attributes\Group;
use App\Http\Controllers\BaseController;
use App\Http\Requests\Auth\LoginRequest;
use Knuckles\Scribe\Attributes\UrlParam;
use Knuckles\Scribe\Attributes\BodyParam;
use Knuckles\Scribe\Attributes\ResponseFromFile;

#[Group('Auth requests')]
class LoginController extends BaseController
{
    public function __construct(
        private readonly LoginService $loginService
    ) {
    }

    /**
     * @param \App\Http\Requests\Auth\LoginRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\AuthenticationException
     */
    #[BodyParam("password", "string", "The password of the user.", example: '123456')]
    #[BodyParam("email", "string", "The email of the user.", example: 'sad_coder@gmail.com')]
    #[ResponseFromFile("responses/auth/login/401.json", 402, description: "incorrect credentials")]
    #[ResponseFromFile("responses/auth/login/200.json", 200, description: "Success auth")]
    public function login(LoginRequest $request): JsonResponse
    {
        $data = $this->loginService->login(
            LoginDto::from($request->validated())
        );

        return $this->respondWithSuccessV1(
            $data->toArray(),
            'success login',
            'user'
        );
    }

}
