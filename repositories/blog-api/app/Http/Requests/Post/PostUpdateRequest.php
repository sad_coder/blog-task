<?php

namespace app\Http\Requests\Post;

use App\Http\Requests\BaseRequest;

class PostUpdateRequest extends BaseRequest
{
    /**
     * @return string[]
     */
    public function rules(): array
    {
        return [
            'title' => 'required_without_all:body|string',
            'body' => 'required_without_all:title|string',
        ];
    }
}
