<?php

namespace App\Http\Requests\Post;

use App\Http\Requests\BaseRequest;

class PostStoreRequest extends BaseRequest
{
    /**
     * @return string[]
     */
    public function rules(): array
    {
        return [
            'title' => 'required|string',
            'body' => 'required|string',
        ];
    }
}
