<?php

namespace tests\Feature\Controllers\Register;

use App\Models\User;
use Illuminate\Testing\Fluent\AssertableJson;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class RegisterTest extends TestCase
{

    /**
     * @var array
     */
    public array $registerData;

    protected function setUp(): void
    {
        parent::setUp();

        $password = fake()->password();
        $this->registerData = [
            'name' => fake()->name(),
            'birthday' => fake()->dateTimeBetween('-60 years', '-18 years')->format('Y-m-d'),
            'email' => fake()->email(),
            'password' => $password,
            'password_confirmation' => $password
        ];
    }

    public function test_that_user_can_register(): void
    {
        $response = $this->postJson(route('auth.register'), $this->registerData);

        $response->assertStatus(Response::HTTP_CREATED);

        $response->assertJsonStructure([
            'message',
            'user' => [
                'name',
                'email',
                'birthday',
                'token_data'=> [
                    'token',
                    'token_type',
                    'expired_at'
                ]
            ]
        ]);

        $this->assertDatabaseHas('users', [
            'email' => $this->registerData['email'],
            'name' => $this->registerData['name'],
            'birthday' => $this->registerData['birthday'],
        ]);
    }
}
