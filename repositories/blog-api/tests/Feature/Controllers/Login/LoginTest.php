<?php

namespace tests\Feature\Controllers\Login;

use App\Models\User;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class LoginTest extends TestCase
{
    public function test_that_user_can_login(): void
    {
        $user = User::factory()->create([
            'password' => 'password'
        ]);

        $response = $this->postJson(route('auth.login'), [
            'email' => $user->email,
            'password' => 'password'
        ]);

        $response->assertStatus(Response::HTTP_OK);

        $response->assertJsonStructure([
            'message',
            'user' => [
                'id',
                'name',
                'email',
                'birthday',
                'token',
            ]
        ]);
    }
}
