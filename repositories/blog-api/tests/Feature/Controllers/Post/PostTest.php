<?php

namespace tests\Feature\Controllers\Register;

use App\Models\User;
use Tests\TestCase;
use App\Models\Post;
use JetBrains\PhpStorm\NoReturn;
use Symfony\Component\HttpFoundation\Response;

class PostTest extends TestCase
{

    /**
     * @var array
     */
    public array $registerData;

    /**
     * @var \App\Models\User
     */
    protected User $user;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->user = User::factory()->create();
    }

    /**
     * @return void
     */
    #[NoReturn] public function test_than_user_can_create_post()
    {
        $createData = [
            'title' => fake()->name(),
            'body' => fake()->text(100),
        ];

        $response = $this->actingAs($this->user,'api')
            ->postJson(route('post.store'), $createData);

        $response->assertStatus(Response::HTTP_CREATED);
        $response->assertJsonStructure([
            'message',
            'post' => [
                'user_id',
                'dummy_post_id',
            ]
        ]);

        $this->assertDatabaseHas('posts', [
            'user_id' => $this->user->id,
            'dummy_post_id' => $response->json('post.dummy_post_id'),
        ]);
    }

    /**
     * @return void
     */
    #[NoReturn] public function test_than_user_can_update_post()
    {
        $post = Post::factory()->create();
        $post->load('user');

        $postBeforeUpdatedDate = $post->updated_at->format('Y-m-d H:i:s');

        $updateData = [
            'title' => fake()->name(),
            'body' => fake()->text(100),
        ];

        $response = $this->actingAs($post->user,'api')
            ->putJson(route('post.update', ['post' => $post->id]), $updateData);

        $response->assertStatus(Response::HTTP_OK);

        $retrievedPost = Post::find($post->id);

        $this->assertNotEquals($postBeforeUpdatedDate, $retrievedPost->updated_at->format('Y-m-d H:i:s'));

        $response->assertJsonStructure([
            'message',
            'post' => [
                'title',
                'body',
            ]
        ]);
    }

    /**
     * @return void
     */
    #[NoReturn] public function test_than_user_can_delete_post()
    {
        $post = Post::factory()->create();
        $post->load('user');

        $response = $this->actingAs($post->user,'api')
            ->delete(route('post.destroy', ['post' => $post->id]));

        $response->assertStatus(Response::HTTP_NO_CONTENT);

        $this->assertDatabaseMissing('posts', ['id' => $post->id]);
    }

    #[NoReturn] public function test_than_user_can_show_post()
    {
        $post = Post::factory()->create();

        $response = $this->actingAs($this->user,'api')
            ->get(route('post.show', ['id' => $post->id]));


        $response->assertStatus(Response::HTTP_OK);

        $response->assertJsonStructure([
            'message',
            'post' => [
                'id',
                'author_name',
                'title',
                'body'
            ]
        ]);

        $this->assertDatabaseHas('posts', ['id' => $post->id]);
    }

    /**
     * @return void
     */
    #[NoReturn] public function test_than_user_can_show_posts()
    {
         Post::factory()->count(10)->create();

        $response = $this->actingAs($this->user,'api')
            ->get(route('post.index'));

        $response->assertStatus(Response::HTTP_OK);

        $response->assertJsonStructure([
            'message',
            'pagination' => [
                'page',
                'by',
                'total'
            ],
            'posts' => [
                '*' => [
                    'id',
                    'user_id',
                    'created_at',
                    'dummy_post_id',
                    'title',
                    'body',
                    'author_name'
                ]
            ]
        ]);

        $this->assertCount(10, $response->json('posts'));
    }

    /**
     * @return void
     */
    #[NoReturn] public function test_than_user_can_not_update_not_own_post()
    {
        $post = Post::factory()->create();
        $post->load('user');

        $anotherUser = User::factory()->create();

        $updateData = [
            'title' => fake()->name(),
            'body' => fake()->text(100),
        ];

        $response = $this->actingAs($anotherUser,'api')
            ->putJson(route('post.update', ['post' => $post->id]), $updateData);

        $response->assertStatus(Response::HTTP_FORBIDDEN);

    }

    /**
     * @return void
     */
    #[NoReturn] public function test_than_user_can_not_delete_not_own_post()
    {
        $post = Post::factory()->create();
        $post->load('user');
        $anotherUser = User::factory()->create();


        $response = $this->actingAs($anotherUser,'api')
            ->delete(route('post.destroy', ['post' => $post->id]));

        $response->assertStatus(Response::HTTP_FORBIDDEN);

    }

}
