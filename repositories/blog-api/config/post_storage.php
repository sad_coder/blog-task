<?php

return [
    'active_service' => env('POST_STORAGE_ACTIVE', 'dummy'),
    'fallback_service' => env('POST_STORAGE_FALLBACK', 'dummy'),

    'dummy' => [
        'api_url' => env('DUMMY_API_URL', 'https://dummyjson.com/'),
    ]
];
