<?php

namespace database\factories;

use App\Models\User;
use App\Models\Post;
use App\Dto\Post\IntegrationPostStoreDto;
use Illuminate\Database\Eloquent\Factories\Factory;
use App\Services\StorageApiIntegration\StorageApiFactory;


class PostFactory extends Factory
{
    /**
     * The current password being used by the factory.
     */
    protected static ?string $password;


    protected $model = Post::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $storageService = StorageApiFactory::getStorageService('dummy');
        $user = User::factory()->create();
        $dummyId = $storageService->storePost(
            IntegrationPostStoreDto::from([
                'title' => fake()->title(),
                'body' => fake()->text(100),
                'userId' => $user->id
            ])
        );

        return [
            'dummy_post_id' => $dummyId,
            'user_id' => $user->id,
        ];
    }

}
