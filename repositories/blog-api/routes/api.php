<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use app\Http\Controllers\Api\V1\Post\PostController;
use app\Http\Controllers\Api\V1\Auth\LoginController;
use app\Http\Controllers\Api\V1\Auth\RegisterController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::group(['prefix' => 'auth'], static function () {
    Route::post('register', [RegisterController::class, 'register'])
        ->name('auth.register');
    Route::post('login', [LoginController::class, 'login'])
        ->name('auth.login');
});

Route::group(['prefix' => 'post'], function () {
    Route::get('/', [PostController::class, 'index'])->name('post.index');
    Route::get('{id}', [PostController::class, 'show'])->name('post.show')
        ->whereNumber('post');
});

Route::group(['middleware' => 'auth:api', 'prefix' => 'post'], function () {
    Route::post('/', [PostController::class, 'store'])->name('post.store');
    Route::put('/{post}', [PostController::class, 'update'])->name('post.update')
        ->whereNumber('post');
    Route::delete('{post}', [PostController::class, 'destroy'])->name('post.destroy')
        ->whereNumber('post');
});
